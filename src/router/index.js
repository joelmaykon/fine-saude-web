import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/pages/login/Login'
import Home from '@/pages/Modulos/Adm/home/Home'


Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
      
    },
    {
      path: '/inicio-fine-administracao',
      name: 'Home',
      component: Home
      
    }
    
    
    
  ]
})
