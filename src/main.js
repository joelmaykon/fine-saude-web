// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueSweetalert2 from 'vue-sweetalert2';
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
// Translation provided by Vuetify (javascript)
import Pt from 'vuetify/es5/locale/pt'
import axios from 'axios';




Vue.prototype.$http = axios
//Vue.prototype.$url = 'http://localhost:5000/api/'
Vue.prototype.$url = 'http://finesaude.com.br/index.php?modulo='
Vue.use(Vuetify,{
  lang: {
    locales: { 'pt': Pt},
    current: 'pt'
  }
})


Vue.config.productionTip = false
Vue.use(VueSweetalert2);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
